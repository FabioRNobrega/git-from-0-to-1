import { Component } from '@angular/core';
import { faList, faCode, faCloud, faChevronDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'myblog-fe';
  faList = faList;
  faChevronDown = faChevronDown;
  faCode = faCode;
  faCloud = faCloud;
  public isCollapsed = false;
}

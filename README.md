# Lesson about Git 2020 | EJCM

This project is the frontend for one ipotetic blog.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

We use:
+ Angular CLI: 7.3.10
+ Node: 11.8.0
+ OS: linux x64
+ Angular: 7.2.16


Table of contents
=================

  * [Install](#install)
  * [Usage](#usage)
  * [Important Libraries](#important-libraries)
  * [Architecture overview](#architecture-overview)

## Install 

+ Clone the repo and cd into myblog-fe

``` bash
$ npm install
```

## Usage 

```bash 
$ ng serve
```

The application will become available at the URL:

```
http://localhost:4200/
```
## Important Libraries

+ [Bootstrap 4.5.2](https://getbootstrap.com/docs/4.5/getting-started/introduction/)
+ [Ng-Bootstrap 4.5](https://github.com/ng-bootstrap/ng-bootstrap#readme)
+ [Font Awesome 0.3](https://github.com/FortAwesome/angular-fontawesome)

## Architecture overview
This is a simple site for a better understanding of the flow, history and use cases of git. 
This project is based on a sass lesson, so if you want to know more about sass visit the [Sass Repo](https://github.com/FabioRNobrega/angular-sass) on gitHub